import Vue from 'vue'
import Router from 'vue-router'

// List
const ListCars = () => import('@/components/list-cars')
const Cars = () => import('@/components/cars')

Vue.use(Router)

export default new Router({
  mode: 'history', // https://router.vuejs.org/api/#model
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '',
      name: 'Car List',
      component: ListCars
    },
    {
      path: 'car/:id',
      name: 'car',
      component: Cars
    },
    {
      path: 'car',
      name: 'car',
      component: Cars
    }
  ]
})
